package utfpr.ct.dainf.if62c.pratica;
/**
 * @author  1906526
 */
public class Jogador implements Comparable<Jogador> {
    private int numero;
    private String nome;
    
    public Jogador(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }
    
    public void setNumero(int numero){
        this.numero = numero;
    }
    public int getNumero() {
        return numero;
    }
    
    public void setNome(String nome){
        this.nome = nome;
    }
    public String getNome() {
        return nome;
    }
    
    @Override
    public String toString() {
        return numero + " - " + nome;
    }

    @Override
    public int compareTo(Jogador jogador) {
        return numero - jogador.numero;
    }
}
