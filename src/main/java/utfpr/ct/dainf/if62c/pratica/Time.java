package utfpr.ct.dainf.if62c.pratica;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
/**
 * @author 1906526
 */
public class Time {
    private HashMap<String,Jogador> jogadores;
    
    public Time(){
        jogadores = new HashMap<>();
    }
    
    public HashMap<String,Jogador> getJogadores(){
        return jogadores;
    }
    
    public void addJogador(String key, Jogador jogador){
        jogadores.put(key, jogador);
    }
    
    public List<Jogador> ordena (JogadorComparator comparador){
        //Collections.sort(List<T> l, Comparator<? super T> c);
        List<Jogador> retorno = new ArrayList<Jogador>(jogadores.values());
        Collections.sort(retorno, comparador);
        return retorno;
    }
}