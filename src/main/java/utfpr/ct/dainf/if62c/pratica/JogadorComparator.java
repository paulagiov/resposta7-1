package utfpr.ct.dainf.if62c.pratica;
import java.util.Comparator;
/**
 * @author 1906526 
 */
public class JogadorComparator implements Comparator<Jogador> {
    private final boolean ordNumero;
    private final boolean ascNumero;
    private final boolean ascNome;
    
    public JogadorComparator(){
        this.ordNumero = true;
        this.ascNumero = true;
        this.ascNome = true;
    }
    public JogadorComparator(boolean ordNumero, boolean ascNumero, boolean ascNome){
        this.ordNumero = ordNumero;
        this.ascNumero = ascNumero;
        this.ascNome = ascNome;
    }
    
    @Override
    ///////GAMBIARRA SEM TAMANHO (VER O DO PEDRO PARA APRENDER)
    public int compare(Jogador player1, Jogador player2){
        //nenhum dos dois eh nulo
        if(player1 != null && player2 != null){
            //ambos iguais
            if(player1.getNome().equals(player2.getNome()) 
            && player1.getNumero() == player2.getNumero()){
                return 0;
            //ordenando por numero///////-------------- desc-asc
            }else if(ordNumero){
                //numero 1 maior que 2
                if(player1.getNumero() > player2.getNumero()){
                    return 1 * (ascNumero?1:-1);
                //numero 2 maior que 1
                }else if(player1.getNumero() < player2.getNumero()){
                    return -1 * (ascNumero?1:-1);
                //numeros iguais, comparar strings
                }else return player1.getNome().compareTo(player2.getNome()) * (ascNome?1:-1);
            }else{
                //nome 1 maior que 2
                if(player1.getNome().compareTo(player2.getNome()) > 0){
                    return 1 * (ascNome?1:-1);
                //nome 2 maior que 1
                }else if(player1.getNome().compareTo(player2.getNome()) < 0){
                    return -1 * (ascNome?1:-1);
                //nomes iguais, comparar numeros
                }else return (((Integer)player1.getNumero()).compareTo(player2.getNumero())) * (ascNumero?1:-1);
            }
        //o 1 eh nulo e o 2 nao
        }else if(player1==null && player2!=null){
            return -1;
        //o 2 eh nulo e o 1 nao
        }else if(player1!=null && player2==null){
            return 1;
        //ambos nulos
        }else return 0;
    }
}
