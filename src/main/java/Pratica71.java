import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
/**
 * @author 1906526
 */
public class Pratica71 {
    public static void main(String[] args) {
        //variavies globais do metodo
        Scanner entrada = new Scanner(System.in).useDelimiter("\n");
        List<Jogador> lista_jogadores = new ArrayList<>();
        int nmro_jogadores = 0;
        boolean entrada_valida = false;
        
        //pegando o numero de jogadores, int e >0
        System.out.println("Digite o número de jogadores:");
        while(!entrada_valida){
            if(entrada.hasNextInt()){
                nmro_jogadores = entrada.nextInt();
                if(nmro_jogadores > 0){                
                    entrada_valida = true;
                }else{
                    System.out.println("Digite um número maior que 0");
                }
            }else{
                entrada.next();
                System.out.println("Digite um número");
            }
        }
        System.out.println();
        
        //rodando e pegando x jogadores
        for(int i=0; i<nmro_jogadores; i++){
            int num_jogador = 0;

            System.out.println("Digite o número do jogador:");
            //numero deve ser int e >0
            entrada_valida = false;
            while(!entrada_valida){
                if(entrada.hasNextInt()){
                    num_jogador = entrada.nextInt();
                    if(num_jogador > 0){                
                        entrada_valida = true;
                    }else{
                        System.out.println("Digite um número maior que 0");
                    }
                }else{
                    entrada.next();
                    System.out.println("Digite um número");
                }
            }
            
            System.out.println("Digite o nome do jogador:");
            //adicionando jogador na lista pegando o nome do teclado
            lista_jogadores.add(new Jogador(num_jogador,entrada.next()));
            System.out.println();
        }
        
        Collections.sort(lista_jogadores,new JogadorComparator(true,true,true));
        System.out.println(lista_jogadores);
        System.out.println();
        
        while(true){
            int num_jogador = 0;
            boolean encerrar = false;
            
            System.out.println("Procedimento para inserir novos jogadores (aperte 0 no numero para parar):");
            System.out.println("Digite o número do jogador:");
            //numero deve ser int e >0, mas se 0, finaliza
            entrada_valida = false;
            while(!entrada_valida){
                if(entrada.hasNextInt()){
                    num_jogador = entrada.nextInt();
                    if(num_jogador == 0){
                        System.out.println("Procedimento encerrado");
                        encerrar = true;
                        break;
                    }else if(num_jogador > 0){                
                        entrada_valida = true;
                    }else{
                        System.out.println("Digite um número maior que 0");
                    }
                }else{
                    entrada.next();
                    System.out.println("Digite um número");
                }
            }
            if(encerrar) break;
            
            System.out.println("Digite o nome do jogador:");
            //adicionando jogador na lista pegando o nome do teclado

            for(Jogador player:lista_jogadores){
                if(player.getNumero() == num_jogador){
                    lista_jogadores.remove(player);
                    break;
                }
            }
            
            lista_jogadores.add(new Jogador(num_jogador,entrada.next()));
            System.out.println();

            Collections.sort(lista_jogadores,new JogadorComparator(true,true,true));
            System.out.println(lista_jogadores);
            System.out.println();
        }
    }
}

